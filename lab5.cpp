/*
Evan Haithcock
ehaithc
Lab 5
Lab Section: 003
Nushrat Humaira
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
   /*This is to seed the random generator */
   srand(unsigned (time(0)));

   /*Create a deck of cards of size 52 and initialize the deck*/
	Card deck[52];
	int counter = 0;
	for(int i = 0; i < 4; i++){
		 for(int j = 2; j < 15; j++){
			deck[counter].suit = static_cast<Suit>(i);
			deck[counter].value = j;
			counter++;
		}
	}

   /*After the deck is created and initialzed we call random_shuffle()*/
	random_shuffle(deck, &deck[52], myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
	Card hand[5];
	for(int i = 0; i < 5; i++){
		 hand[i] = deck[i];
	 }

   /*Sort the cards.  Links to how to call this function is in the specs
    *provided*/
	sort(&hand[0], &hand[5], suit_order);

   /*Now print the hand below. You will use the functions get_card_name and
    *get_suit_code */
	string suit, name;

	for(int i = 0; i < 5; i++){
		 suit = get_suit_code(hand[i]);
		 name = get_card_name(hand[i]);

		 cout << setw(10) << name << " of " << suit << endl;
	}

   return 0;
}


/*This function will be passed to the sort funtion.*/
bool suit_order(const Card& lhs, const Card& rhs){
   bool lSmaller = false;
   if(lhs.suit < rhs.suit){
	    lSmaller = true;
   }
   else if(lhs.suit == rhs.suit){
	    if(lhs.value < rhs.value){
		     lSmaller = true;
	    }
   }

   return lSmaller;
}

/*Gets the code to print out the symbol of the suit*/
string get_suit_code(Card& c){
   switch (c.suit) {
     case SPADES:    return "\u2660";
     case HEARTS:    return "\u2661";
     case DIAMONDS:  return "\u2662";
     case CLUBS:     return "\u2663";
     default:        return "";
   }
}

/*Gets the name of the card to print out.*/
string get_card_name(Card& c){
   switch (c.value){
	  case 2:	return "2";
	  case 3:	return "3";
	  case 4: 	return "4";
	  case 5:	return "5";
	  case 6:	return "6";
	  case 7:	return "7";
	  case 8:	return "8";
	  case 9:	return "9";
	  case 10:	return "10";
	  case 11:	return "Jack";
	  case 12:	return "Queen";
	  case 13:	return "King";
	  case 14:	return "Ace";
	  default:	return "";
	}
}
